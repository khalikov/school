# README #

This is a repository for task Courses (School)

### Technology ###

* Backend - Django Rest Framework

### How to set up? ###

* cd school
* virtualenv SCHOOL
* source SCHOOL/bin/activate
* pip install -r requirements.txt
* python manage.py migrate
* python manage.py loaddata klass.json
* python manage.py runserver
* go to 127.0.0.1:8000, there will be GUI for backend from Django Rest Framework