from django.db import models
from django.contrib.auth.models import AbstractUser
# Create your models here.


class User(AbstractUser):
    ROLE_TEACHER = 1
    ROLE_STUDENT = 2

    def __unicode__(self):
        return u'%s %s' % (self.first_name, self.last_name)

class Specialization(models.Model):
    name = models.CharField('Specialization', max_length=150)
    teachers = models.ManyToManyField('User',
                                      related_name='specializations',
                                      blank=True)

    def __unicode__(self):
        return self.name


class Klass(models.Model):
    name = models.CharField('Class Name', max_length=150)
    description = models.TextField('Class Description', blank=True, default='')
    specialization = models.ForeignKey(Specialization)
    teacher = models.ForeignKey(User, verbose_name='Teacher', null=True)

