from django.contrib import admin
from klass.models import User, Specialization
# Register your models here.
admin.site.register(User)
admin.site.register(Specialization)