from rest_framework.filters import SearchFilter
from klass.models import User, Klass
from rest_framework import viewsets
from klass.serializers import (
    TeacherSerializer, StudentSerializer, KlassSerializer
)


class TeacherViewSet(viewsets.ModelViewSet):
    base_name = 'teachers'
    filter_backends = (SearchFilter, )
    filter_fields = ('first_name', 'last_name', 'specializations__name',
                     'specializations__id')
    search_fields = ('first_name', 'last_name', 'specializations__name',
                     '=specializations__id')

    def get_queryset(self):
        queryset = User.objects.filter(
            groups__id=User.ROLE_TEACHER)
        queryset = queryset.prefetch_related('groups', 'specializations')
        queryset = queryset.order_by('first_name', 'last_name')
        return queryset

    serializer_class = TeacherSerializer


class StudentViewSet(viewsets.ModelViewSet):
    base_name = 'students'
    filter_backends = (SearchFilter, )
    filter_fields = ('first_name', 'last_name')
    search_fields = filter_fields
    serializer_class = StudentSerializer

    def get_queryset(self):
        queryset = User.objects.filter(
            groups__id=User.ROLE_STUDENT)
        queryset = queryset.prefetch_related('groups')
        queryset = queryset.order_by('first_name', 'last_name')
        return queryset


class KlassViewSet(viewsets.ModelViewSet):
    filter_backends = (SearchFilter, )
    filter_fields = ('name', 'description', 'specialization__name',
                     'specialization__id')
    search_fields = ('name', 'description', 'specialization__name',
                     '=specialization__id')
    serializer_class = KlassSerializer

    def get_queryset(self):
        queryset = Klass.objects.all()
        queryset = queryset.prefetch_related('specialization')
        queryset = queryset.order_by('name')
        return queryset