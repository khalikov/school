import hashlib
import datetime
from rest_framework.exceptions import ValidationError
from rest_framework.fields import CharField
from rest_framework.relations import PrimaryKeyRelatedField
from klass.models import User, Specialization, Klass
from rest_framework import serializers


class SpecializationSerializer(serializers.ModelSerializer):
    class Meta:
        model = Specialization
        fields = (
            'name'
        )


def get_password(validated_data):
    if False:
        # generation of temporary password
        m = hashlib.md5()
        m.update('%s_%s' % (validated_data['email'],
                            datetime.datetime.now().strftime('%Y%m%d')))
        return m.hexdigest()
    return validated_data['email']


class TeacherSerializer(serializers.HyperlinkedModelSerializer):
    specializations = PrimaryKeyRelatedField(
        queryset=Specialization.objects.all(), many=True)
    lookup_field = 'id'
    # password = CharField(write_only=True)
    email = CharField(write_only=True, max_length=150)

    class Meta:
        model = User
        fields = ('url', 'email',
                  'first_name', 'last_name', 'specializations')

    def create(self, validated_data):
        validated_data['username'] = validated_data['email']
        validated_data['password'] = get_password(validated_data)
        specializations = validated_data.pop('specializations', [])
        user = User.objects.create_user(**validated_data)
        user.groups = [User.ROLE_TEACHER]
        user.specializations = specializations
        return user

    def update(self, instance, validated_data):
        if validated_data['email']:
            validated_data['username'] = validated_data['email']
        return super(TeacherSerializer, self).update(instance, validated_data)

    def build_url_field(self, field_name, model_class):
        """
        Create a field representing the object's own URL.
        """
        field_class = self.serializer_url_field
        field_kwargs = {
            'view_name': 'teachers-detail'
        }

        return field_class, field_kwargs


class StudentSerializer(serializers.HyperlinkedModelSerializer):
    email = CharField(write_only=True, max_length=150)
    class Meta:
        model = User
        fields = ('url', 'email', 'first_name', 'last_name')

    def update(self, instance, validated_data):
        if validated_data['email']:
            validated_data['username'] = validated_data['email']
        return super(StudentSerializer, self).update(instance, validated_data)


    def create(self, validated_data):

        validated_data['username'] = validated_data['email']
        validated_data['password'] = get_password(validated_data)
        if User.objects.filter(username=validated_data['username']).exists():
            raise ValidationError({'email': 'Not Unique'})
        user = User.objects.create_user(**validated_data)
        user.groups = [User.ROLE_STUDENT]
        return user

    def build_url_field(self, field_name, model_class):
        """
        Create a field representing the object's own URL.
        """
        field_class = self.serializer_url_field
        field_kwargs = {
            'view_name': 'students-detail'
        }
        return field_class, field_kwargs


class KlassSerializer(serializers.HyperlinkedModelSerializer):
    specialization = PrimaryKeyRelatedField(
        queryset=Specialization.objects.all())
    teacher = PrimaryKeyRelatedField(
        queryset=User.objects.filter(groups__id=User.ROLE_TEACHER)
    )

    def check_teacher(self, validated_data, instance=None):
        teacher = validated_data['teacher']
        if not teacher and instance is not None:
            teacher = instance.teacher
        if teacher:
            try:
                User.objects.get(id=teacher.id,
                                 groups__id=User.ROLE_TEACHER)
            except User.DoesNotExist:
                raise ValidationError({'teacher': 'Does not exist'})

    def create(self, validated_data):
        self.check_teacher(validated_data)
        return super(KlassSerializer, self).create(validated_data)

    def update(self, instance, validated_data):
        self.check_teacher(validated_data, instance=instance)
        return super(KlassSerializer, self).update(instance, validated_data)

    class Meta:
        model = Klass
        fields = ('url', 'name', 'description', 'specialization', 'teacher')
